﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;

namespace GA
{
    public partial class Form1 : Form
    {
        //Создание экземпляра класса
        GAMethods gaMethods;

        public Form1()
        {
            InitializeComponent();
            for (int i = 1; i <= Convert.ToInt32(cbCountPC.Text); i++ )
            {
                cbStartPC.Items.Add(Convert.ToString(i));
                cbFinishPC.Items.Add(Convert.ToString(i));
            }

            for (int i = 1; i < Convert.ToInt32(cbCountPC.Text); i++)
            {
                cbCross.Items.Add(i.ToString());
            }

            


        }

        // Начало работы
        private void buStart_Click(object sender, EventArgs e)
        {
            //Получение настроек алгоритма
            int PCs = Convert.ToInt32(cbCountPC.Text);
            int startPc = Convert.ToInt32(cbStartPC.Text);
            int finishPc = Convert.ToInt32(cbFinishPC.Text);
            int populSize = Convert.ToInt32(tbPopulSize.Text);
            int cross = Convert.ToInt32(cbCross.Text);
            int mut = Convert.ToInt32(tbMut.Text);

            gaMethods = new GAMethods(PCs, startPc, finishPc, populSize, cross, mut);

            // Заполнение таблицы
            int[][] matrix = gaMethods.GetMatrix();
            
            gridMatrix.RowCount = PCs + 1;
            gridMatrix.ColumnCount = PCs + 1;

            for (int i = 0; i < PCs + 1; i++)
            {
                for (int j = 0; j <= PCs; j++)
                {
                    if (i == 0 && j == 0)
                    {
                        gridMatrix.Rows[i].Cells[j].Value = "Компьютер";
                    }
                    else if (i == 0)
                    {
                        gridMatrix.Rows[i].Cells[j].Value = j;
                    }
                    else if (j == 0)
                    {
                        gridMatrix.Rows[i].Cells[j].Value = i;
                    }
                    else
                    {
                        gridMatrix.Rows[i].Cells[j].Value = matrix[i -1][j - 1];
                    }
                }
            }

            //Начальная поуляция
            int[][] populaationStart = gaMethods.GetPopulation();
            var (fits, populs) = gaMethods.GetFitness(populaationStart);
            var stringToShow = gaMethods.GetStr(fits, populs);
            textStart.Text = stringToShow.ToString();

            //Промежуточная популяция
            int[][] populationIntermediate = gaMethods.GetIntermediate(fits, populs);
            var (fitsI, populsI) = gaMethods.GetFitness(populationIntermediate);
            var stringToShowIntermediate = gaMethods.GetStr(fitsI, populsI);
            textInterval.Text = stringToShowIntermediate.ToString();

            //Финальная популяция
            int[][] finalPopulation = gaMethods.GetFinalPopulation(fits, populs, fitsI, populsI);
            var (fitsF, populsF) = gaMethods.GetFitness(finalPopulation);
            var stringToShowFinal = gaMethods.GetStr(fitsF, populsF);
            textFinal.Text = stringToShowFinal.ToString();

        }

        private void buStep_Click(object sender, EventArgs e)
        {

            var fits = gaMethods.fits;
            var populs = gaMethods.populs;
            var stringToShow = gaMethods.GetStr(fits, populs);
            textStart.Text = stringToShow.ToString();

            int[][] populationIntermediate = gaMethods.GetIntermediate(fits, populs);
            var (fitsI, populsI) = gaMethods.GetFitness(populationIntermediate);
            var stringToShowIntermediate = gaMethods.GetStr(fitsI, populsI);
            textInterval.Text = stringToShowIntermediate.ToString();

            //Финальная популяция
            int[][] finalPopulation = gaMethods.GetFinalPopulation(fits, populs, fitsI, populsI);
            var (fitsF, populsF) = gaMethods.GetFitness(finalPopulation);
            var stringToShowFinal = gaMethods.GetStr(fitsF, populsF);
            textFinal.Text = stringToShowFinal.ToString();

            
        }

        private void cbCountPC_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbStartPC.Items.Clear();
            cbFinishPC.Items.Clear();
            cbCross.Items.Clear();
            for (int i = 1; i <=  Convert.ToInt32(cbCountPC.Text); i++)
            {
                cbStartPC.Items.Add(Convert.ToString(i));
                cbFinishPC.Items.Add(Convert.ToString(i));
            }

            for (int i = 1; i < Convert.ToInt32(cbCountPC.Text); i++)
            {
                cbCross.Items.Add(i.ToString());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (rbFinal.Checked)
            {
                int swears = Convert.ToInt32(tbSwearing.Text);
                for (int i = 0; i < swears; i++)
                {
                    var fits = gaMethods.fits;
                    var populs = gaMethods.populs;
                    var stringToShow = gaMethods.GetStr(fits, populs);
                    textStart.Text = stringToShow.ToString();

                    int[][] populationIntermediate = gaMethods.GetIntermediate(fits, populs);
                    var (fitsI, populsI) = gaMethods.GetFitness(populationIntermediate);
                    var stringToShowIntermediate = gaMethods.GetStr(fitsI, populsI);
                    textInterval.Text = stringToShowIntermediate.ToString();

                    //Финальная популяция
                    int[][] finalPopulation = gaMethods.GetFinalPopulation(fits, populs, fitsI, populsI);
                    var (fitsF, populsF) = gaMethods.GetFitness(finalPopulation);
                    var stringToShowFinal = gaMethods.GetStr(fitsF, populsF);
                    textFinal.Text = stringToShowFinal.ToString();
                }
            }           
        }
    }
}

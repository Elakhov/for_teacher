﻿namespace GA
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbPopulSize = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbSwearing = new System.Windows.Forms.TextBox();
            this.cbCountPC = new System.Windows.Forms.ComboBox();
            this.buStart = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cbStartPC = new System.Windows.Forms.ComboBox();
            this.cbFinishPC = new System.Windows.Forms.ComboBox();
            this.gridMatrix = new System.Windows.Forms.DataGridView();
            this.textStart = new System.Windows.Forms.TextBox();
            this.textInterval = new System.Windows.Forms.TextBox();
            this.textFinal = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbFinal = new System.Windows.Forms.RadioButton();
            this.rbStep = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.buStep = new System.Windows.Forms.Button();
            this.cbCross = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbMut = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gridMatrix)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(196, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(207, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Количество компьютеров";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(409, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(149, 20);
            this.label3.TabIndex = 7;
            this.label3.Text = "Размер популяции";
            // 
            // tbPopulSize
            // 
            this.tbPopulSize.Location = new System.Drawing.Point(413, 35);
            this.tbPopulSize.Name = "tbPopulSize";
            this.tbPopulSize.Size = new System.Drawing.Size(100, 20);
            this.tbPopulSize.TabIndex = 9;
            this.tbPopulSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(433, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(185, 20);
            this.label4.TabIndex = 10;
            this.label4.Text = "Количество поколений";
            // 
            // tbSwearing
            // 
            this.tbSwearing.Location = new System.Drawing.Point(518, 124);
            this.tbSwearing.Name = "tbSwearing";
            this.tbSwearing.Size = new System.Drawing.Size(100, 20);
            this.tbSwearing.TabIndex = 11;
            // 
            // cbCountPC
            // 
            this.cbCountPC.FormattingEnabled = true;
            this.cbCountPC.Items.AddRange(new object[] {
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.cbCountPC.Location = new System.Drawing.Point(204, 36);
            this.cbCountPC.Name = "cbCountPC";
            this.cbCountPC.Size = new System.Drawing.Size(50, 21);
            this.cbCountPC.TabIndex = 15;
            this.cbCountPC.Text = "4";
            this.cbCountPC.SelectedIndexChanged += new System.EventHandler(this.cbCountPC_SelectedIndexChanged);
            // 
            // buStart
            // 
            this.buStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buStart.Location = new System.Drawing.Point(714, 103);
            this.buStart.Name = "buStart";
            this.buStart.Size = new System.Drawing.Size(70, 41);
            this.buStart.TabIndex = 16;
            this.buStart.Text = "Начать";
            this.buStart.UseVisualStyleBackColor = true;
            this.buStart.Click += new System.EventHandler(this.buStart_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(201, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Начальный";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(268, 64);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "Конечный";
            // 
            // cbStartPC
            // 
            this.cbStartPC.FormattingEnabled = true;
            this.cbStartPC.Location = new System.Drawing.Point(204, 81);
            this.cbStartPC.Name = "cbStartPC";
            this.cbStartPC.Size = new System.Drawing.Size(47, 21);
            this.cbStartPC.TabIndex = 19;
            // 
            // cbFinishPC
            // 
            this.cbFinishPC.FormattingEnabled = true;
            this.cbFinishPC.Location = new System.Drawing.Point(271, 81);
            this.cbFinishPC.Name = "cbFinishPC";
            this.cbFinishPC.Size = new System.Drawing.Size(42, 21);
            this.cbFinishPC.TabIndex = 20;
            // 
            // gridMatrix
            // 
            this.gridMatrix.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.gridMatrix.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridMatrix.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridMatrix.Location = new System.Drawing.Point(12, 150);
            this.gridMatrix.Name = "gridMatrix";
            this.gridMatrix.ReadOnly = true;
            this.gridMatrix.RowHeadersWidth = 20;
            this.gridMatrix.Size = new System.Drawing.Size(614, 356);
            this.gridMatrix.TabIndex = 22;
            // 
            // textStart
            // 
            this.textStart.Location = new System.Drawing.Point(632, 150);
            this.textStart.Multiline = true;
            this.textStart.Name = "textStart";
            this.textStart.Size = new System.Drawing.Size(228, 115);
            this.textStart.TabIndex = 23;
            // 
            // textInterval
            // 
            this.textInterval.Location = new System.Drawing.Point(632, 271);
            this.textInterval.Multiline = true;
            this.textInterval.Name = "textInterval";
            this.textInterval.Size = new System.Drawing.Size(228, 115);
            this.textInterval.TabIndex = 24;
            // 
            // textFinal
            // 
            this.textFinal.Location = new System.Drawing.Point(632, 392);
            this.textFinal.Multiline = true;
            this.textFinal.Name = "textFinal";
            this.textFinal.Size = new System.Drawing.Size(228, 115);
            this.textFinal.TabIndex = 25;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbFinal);
            this.groupBox2.Controls.Add(this.rbStep);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(178, 117);
            this.groupBox2.TabIndex = 27;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Режим работы";
            // 
            // rbFinal
            // 
            this.rbFinal.AutoSize = true;
            this.rbFinal.Location = new System.Drawing.Point(6, 52);
            this.rbFinal.Name = "rbFinal";
            this.rbFinal.Size = new System.Drawing.Size(110, 24);
            this.rbFinal.TabIndex = 1;
            this.rbFinal.TabStop = true;
            this.rbFinal.Text = "Популяция";
            this.rbFinal.UseVisualStyleBackColor = true;
            // 
            // rbStep
            // 
            this.rbStep.AutoSize = true;
            this.rbStep.Location = new System.Drawing.Point(6, 21);
            this.rbStep.Name = "rbStep";
            this.rbStep.Size = new System.Drawing.Size(56, 24);
            this.rbStep.TabIndex = 0;
            this.rbStep.TabStop = true;
            this.rbStep.Text = "Шаг";
            this.rbStep.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(600, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 20);
            this.label1.TabIndex = 28;
            this.label1.Text = "Кроссовер";
            // 
            // buStep
            // 
            this.buStep.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buStep.Location = new System.Drawing.Point(790, 103);
            this.buStep.Name = "buStep";
            this.buStep.Size = new System.Drawing.Size(70, 41);
            this.buStep.TabIndex = 30;
            this.buStep.Text = "Шаг";
            this.buStep.UseVisualStyleBackColor = true;
            this.buStep.Click += new System.EventHandler(this.buStep_Click);
            // 
            // cbCross
            // 
            this.cbCross.FormattingEnabled = true;
            this.cbCross.Location = new System.Drawing.Point(604, 33);
            this.cbCross.Name = "cbCross";
            this.cbCross.Size = new System.Drawing.Size(121, 21);
            this.cbCross.TabIndex = 31;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(742, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 20);
            this.label5.TabIndex = 32;
            this.label5.Text = "Мутация";
            // 
            // tbMut
            // 
            this.tbMut.Location = new System.Drawing.Point(746, 33);
            this.tbMut.Name = "tbMut";
            this.tbMut.Size = new System.Drawing.Size(100, 20);
            this.tbMut.TabIndex = 33;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(624, 101);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(84, 43);
            this.button1.TabIndex = 34;
            this.button1.Text = "Получить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(872, 518);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tbMut);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cbCross);
            this.Controls.Add(this.buStep);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.textFinal);
            this.Controls.Add(this.textInterval);
            this.Controls.Add(this.textStart);
            this.Controls.Add(this.gridMatrix);
            this.Controls.Add(this.cbFinishPC);
            this.Controls.Add(this.cbStartPC);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.buStart);
            this.Controls.Add(this.cbCountPC);
            this.Controls.Add(this.tbSwearing);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbPopulSize);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Name = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.gridMatrix)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbPopulSize;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbSwearing;
        private System.Windows.Forms.ComboBox cbCountPC;
        private System.Windows.Forms.Button buStart;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbStartPC;
        private System.Windows.Forms.ComboBox cbFinishPC;
        private System.Windows.Forms.DataGridView gridMatrix;
        private System.Windows.Forms.TextBox textStart;
        private System.Windows.Forms.TextBox textInterval;
        private System.Windows.Forms.TextBox textFinal;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbFinal;
        private System.Windows.Forms.RadioButton rbStep;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buStep;
        private System.Windows.Forms.ComboBox cbCross;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbMut;
        private System.Windows.Forms.Button button1;
    }
}


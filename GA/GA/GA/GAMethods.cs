﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace GA
{
    class GAMethods
    {
        int PCs;
        int startPC;
        int finishPC;
        int populSize;
        int cross;
        int mut;
        Random rand = new Random();

        //Матрица переходов
        public int[][] matrix;
        //Матрица переходов для изменения
        public int[][] matrixToChange;
        //Популяция
        //public int[][] population;
        //Промежуточная популяция
        public int[][] populationIntermediate;

        //Словарь для хранения фитнес функций вида H1: 45 популяции
        public Dictionary<string, int> fits;
        //Словарь для хранения популяций вида: H1: [2, 3, 2, 5, 7] популяции
        public Dictionary<string, int[]> populs;
        //Динамический массив для хранения средних фитнесс-функций
        public ArrayList listOfFits = new ArrayList();

        public GAMethods(int PCs, int startPC, int finishPC, int populSize, int cross, int mut)
        {
            this.PCs = PCs;
            this.startPC = startPC;
            this.finishPC = finishPC;
            this.populSize = populSize;
            this.cross = cross;
            this.mut = mut;
        }

        //СОЗДАНИЕ МАТРИЦЫ ПЕРЕХОДОВ
        public int[][] GetMatrix()
        {
            Random rand = new Random();
            //Матрица переходов 
            //PC - количетсво еомпьютеров
            int[][] Matrix = new int[PCs][];
            for (int i = 0; i < PCs; i++)
            {
                Matrix[i] = new int[PCs];
                for (int j = 0; j < PCs; j++)
                {
                    if (i == j)
                    {
                        Matrix[i][j] = 0;
                    }
                    else
                    {
                        int temp = rand.Next(20, 61);
                        Matrix[i][j] = temp;
                    }
                }

            }

            for (int i = 0; i < PCs; i++)
            {
                for (int j = 0; j < PCs; j++)
                {
                    Matrix[j][i] = Matrix[i][j];
                }
            }
            matrix = Matrix;
            matrixToChange = Matrix;
            return Matrix;
        }
        

        //ГЕНЕРАЦИЯ ПОПУЛЯЦИИ
        public int[][] GetPopulation()
        {

            Random rand = new Random();

            int[][] population = new int[populSize][];

            for (int i = 0; i < populSize; i++)
            {
                population[i] = new int[PCs];
                for (int j = 0; j < PCs; j++)
                {
                    if (j == 0 ){
                        population[i][j] = startPC; 
                    }
                    else if (j == PCs - 1)
                    {
                        population[i][j] = finishPC;
                    }
                    else
                    {
                        population[i][j] = rand.Next(1, PCs + 1);
                    }
                }
            }
            return population;
        }


        //ВЫЧИСЛЕНИЕ ФУНКЦИЙ ПРЕСПОСОБЛЕНОСТИ
        public (Dictionary<string, int>, Dictionary<string, int[]>) GetFitness(int[][] population)
        {
            //Словарь для хранения значения фитнесс-функций
            Dictionary<string, int> fits = new Dictionary<string, int>();
            //Словарь для хранения популяции
            Dictionary<string, int[]> populs = new Dictionary<string, int[]>();
            
            //Считаем фитнесс-функцию для каждого решения
            for (int i = 0; i < population.Length; i++)
            {
                int fitFunc = 0;
                for (int j = 0; j < population[i].Length; j++)
                {
                    if (j == population[i].Length - 1)
                    {
                        break;
                    }
                    int pc1 = population[i][j];
                    int pc2 = population[i][j + 1];
                    fitFunc += matrixToChange[pc1 - 1][pc2 - 1];
                }
                //Заполнение словарей
                string tempStr = "H" + (i + 1);
                fits.Add(tempStr, fitFunc);
                populs.Add(tempStr, population[i]);
            }
            //Сортировка словаря
            fits = fits.OrderBy(pair => pair.Value).ToDictionary(pair => pair.Key, pair => pair.Value);
            this.fits = fits;
            this.populs = populs;
            return (fits, populs);
        }


        //ПОЛУЧЕНИЕ СТРОКИ ВЫВОДА ДЛЯ ПОПУЛЯЦИИ
        public StringBuilder GetStr(Dictionary<string, int> fits, Dictionary<string, int[]> populs)
        {
            //Конструктор строки для вывода популяции
            StringBuilder buildStr = new StringBuilder();

            //Формирование строки вывода
            foreach (KeyValuePair<string, int> keyValue in fits)
            {
                string exemp = keyValue.Key;
                int fit = keyValue.Value;
                int[] populArr = populs[exemp];
                string populStr = "";

                for (int i = 0; i < populArr.Length; i++)
                {
                    populStr += populArr[i] + " ";
                }
                string finalStr = String.Format("{0}: [{1}] F={2}", exemp, populStr, fit);
                buildStr.AppendLine(finalStr);
            }
            return buildStr;
        }


        //ПОЛУЧЕНИЕ ПРОМЕЖУТОЧНОЙ ПОПУЛЯЦИИ
        public int[][] GetIntermediate(
            Dictionary<string, int> fits, Dictionary<string, int[]> populs)
        {
            //Длина кромосомы
            int chromeLength = PCs;

            int[][] arrayOfChrome = new int[populSize][];
            int[][] tempArrInter = new int[arrayOfChrome.Length][];

            //Достать хромосомы из словаря
            int i = 0;
            foreach (string key in fits.Keys)
            {
                arrayOfChrome[i] = populs[key];
                i++;
            }

            int iterCount = 0;
            //Обмен генами
             for (int j = 0; j < populSize / 2; j++)
            {
                int[] temp1;
                int[] temp2;
                //Хромосомы первой популяции
                if (j == 0)
                {
                    temp1 = arrayOfChrome[0];
                    temp2 = arrayOfChrome[j + 1];
                }
                else
                {
                    temp1 = arrayOfChrome[iterCount - 2];
                    temp2 = arrayOfChrome[iterCount - 1];
                }

                //Хромосомы промежуточной популяции
                int[] temp3 = new int[chromeLength];
                int[] temp4 = new int[chromeLength];

                for (int k = 0; k < cross; k++)
                {
                    temp3[k] = temp1[k];
                    temp4[k] = temp2[k];
                    
                }

                for (int s = cross; s < chromeLength; s++)
                {
                    temp3[s] = temp2[s];
                    temp4[s] = temp1[s];
                    
                }
                iterCount += 2;

                if (j == 0)
                {
                    tempArrInter[j] = temp3;
                    tempArrInter[j + 1] = temp4;
                }
                else
                {
                    tempArrInter[iterCount - 2] = temp3;
                    tempArrInter[iterCount - 1] = temp4;
                }
            }

            //Добавить первую хромосому при нечетном кол-ве
            if (populSize % 2 != 0)
            {
                tempArrInter[populSize - 1] = arrayOfChrome[0]; 
            }

            int verTooMut = 100 / mut;

            int choicMut = rand.Next(1, verTooMut + 1);

            if (choicMut == verTooMut)
            {
                tempArrInter[rand.Next(0, populSize)][rand.Next(1, PCs - 1)] = rand.Next(startPC, PCs + 1);
                tempArrInter[rand.Next(0, populSize)][rand.Next(1, PCs - 1)] = rand.Next(1, PCs + 1);
                tempArrInter[rand.Next(0, populSize)][rand.Next(1, PCs - 1)] = rand.Next(1, PCs + 1);
                tempArrInter[rand.Next(0, populSize)][rand.Next(1, PCs - 1)] = rand.Next(1, PCs + 1);
            }


            return tempArrInter;
        }


        //ПОЛУЧЕНИЕ ФИНАЛЬНОЙ ПОПУЛЯЦИИ
        public int[][] GetFinalPopulation(
            Dictionary<string, int> fits, Dictionary<string, int[]> populs, Dictionary<string, int> fitsI, Dictionary<string, int[]> populsI)
        {
            //Словари для финальной популяции
            Dictionary<string, int> fitsTempF = new Dictionary<string, int>();
            Dictionary<string, int[]> populsTempF = new Dictionary<string, int[]>();

            int count = fits.Count;

            //Добавление начального словаря в общий
            foreach (string exemp in fits.Keys)
            {
                fitsTempF.Add(exemp, fits[exemp]);
                populsTempF.Add(exemp, populs[exemp]);
            }

            //Добавление промежуточного словаря в общий
            foreach (string exemp in fitsI.Keys)
            {
                string tempExemp = exemp;
                int i = 1;

                //Если ключ уже есть в словаре то меняем название ключа
                while (fitsTempF.ContainsKey(tempExemp))
                {
                    tempExemp = "H" + (i);
                    i++;
                }

                fitsTempF.Add(tempExemp, fitsI[exemp]);
                populsTempF.Add(tempExemp, populsI[exemp]);
            }

            fitsTempF = fitsTempF.OrderBy(pair => pair.Value).Distinct().ToDictionary(pair => pair.Key, pair => pair.Value);

            while (fitsTempF.Count < count)
            {
                foreach (string exemp in fits.Keys)
                {
                    string tempExemp = exemp;
                    int i = 1;
                    while (fitsTempF.ContainsKey(tempExemp))
                    {
                        tempExemp = "H" + (i);
                        i++;
                    }

                    fitsTempF.Add(tempExemp, fits[exemp]);
                    populsTempF.Add(tempExemp, populs[exemp]);
                }
            }

            fitsTempF = fitsTempF.OrderBy(pair => pair.Value).ToDictionary(pair => pair.Key, pair => pair.Value);

            Dictionary<string, int> fitsF = new Dictionary<string, int>();
            Dictionary<string, int[]> populsF = new Dictionary<string, int[]>();

            int j = 0;
            foreach (string exemp in fitsTempF.Keys)
            {
                if (j == count) break;
                fitsF.Add(exemp, fitsTempF[exemp]);
                populsF.Add(exemp, populsTempF[exemp]);
                j++;
            }

            int[][] finalPopul = new int[count][];
            //Занесение популяции в массив, и подсчет суммы функций
            int count1 = 0;
            int sumOfFits = 0;
            foreach (string exemp in fitsF.Keys)
            {
                if (count1 == count) break;
                finalPopul[count1] = populsF[exemp];
                sumOfFits += fitsF[exemp];
                count1++;
            }

            listOfFits.Add(sumOfFits);

            return finalPopul;
        }
    }
}

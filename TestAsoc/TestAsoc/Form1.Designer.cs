﻿namespace TestAsoc
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.buStartTest = new System.Windows.Forms.Button();
            this.laInfo = new System.Windows.Forms.Label();
            this.buNextWord = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.laWord = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.buSave = new System.Windows.Forms.Button();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.laName = new System.Windows.Forms.Label();
            this.laSName = new System.Windows.Forms.Label();
            this.laAge = new System.Windows.Forms.Label();
            this.laGender = new System.Windows.Forms.Label();
            this.textAge = new System.Windows.Forms.TextBox();
            this.comboGender = new System.Windows.Forms.ComboBox();
            this.textSName = new System.Windows.Forms.TextBox();
            this.textName = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // buStartTest
            // 
            this.buStartTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buStartTest.Location = new System.Drawing.Point(132, 259);
            this.buStartTest.Name = "buStartTest";
            this.buStartTest.Size = new System.Drawing.Size(121, 53);
            this.buStartTest.TabIndex = 2;
            this.buStartTest.Text = "Начать";
            this.buStartTest.UseVisualStyleBackColor = true;
            this.buStartTest.Click += new System.EventHandler(this.buStartTest_Click);
            // 
            // laInfo
            // 
            this.laInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laInfo.Location = new System.Drawing.Point(80, 48);
            this.laInfo.Name = "laInfo";
            this.laInfo.Size = new System.Drawing.Size(238, 53);
            this.laInfo.TabIndex = 3;
            this.laInfo.Text = "Вводите ассоциации на предложенные слова";
            this.laInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buNextWord
            // 
            this.buNextWord.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buNextWord.Location = new System.Drawing.Point(132, 259);
            this.buNextWord.Name = "buNextWord";
            this.buNextWord.Size = new System.Drawing.Size(121, 53);
            this.buNextWord.TabIndex = 4;
            this.buNextWord.Text = "Далее";
            this.buNextWord.UseVisualStyleBackColor = true;
            this.buNextWord.Click += new System.EventHandler(this.buNextWord_Click);
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox1.Location = new System.Drawing.Point(117, 137);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(163, 23);
            this.textBox1.TabIndex = 0;
            // 
            // laWord
            // 
            this.laWord.AutoSize = true;
            this.laWord.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laWord.ForeColor = System.Drawing.Color.Black;
            this.laWord.Location = new System.Drawing.Point(164, 98);
            this.laWord.Name = "laWord";
            this.laWord.Size = new System.Drawing.Size(62, 20);
            this.laWord.TabIndex = 6;
            this.laWord.Text = "Слово";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // buSave
            // 
            this.buSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buSave.Location = new System.Drawing.Point(132, 259);
            this.buSave.Name = "buSave";
            this.buSave.Size = new System.Drawing.Size(121, 53);
            this.buSave.TabIndex = 7;
            this.buSave.Text = "Сохранить";
            this.buSave.UseVisualStyleBackColor = true;
            this.buSave.Click += new System.EventHandler(this.buSave_Click);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // laName
            // 
            this.laName.AutoSize = true;
            this.laName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laName.Location = new System.Drawing.Point(103, 35);
            this.laName.Name = "laName";
            this.laName.Size = new System.Drawing.Size(40, 20);
            this.laName.TabIndex = 8;
            this.laName.Text = "Имя";
            // 
            // laSName
            // 
            this.laSName.AutoSize = true;
            this.laSName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laSName.Location = new System.Drawing.Point(62, 65);
            this.laSName.Name = "laSName";
            this.laSName.Size = new System.Drawing.Size(81, 20);
            this.laSName.TabIndex = 9;
            this.laSName.Text = "Фамилия";
            // 
            // laAge
            // 
            this.laAge.AutoSize = true;
            this.laAge.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laAge.Location = new System.Drawing.Point(71, 121);
            this.laAge.Name = "laAge";
            this.laAge.Size = new System.Drawing.Size(72, 20);
            this.laAge.TabIndex = 10;
            this.laAge.Text = "Возраст";
            // 
            // laGender
            // 
            this.laGender.AutoSize = true;
            this.laGender.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laGender.Location = new System.Drawing.Point(103, 95);
            this.laGender.Name = "laGender";
            this.laGender.Size = new System.Drawing.Size(40, 20);
            this.laGender.TabIndex = 11;
            this.laGender.Text = "Пол";
            this.laGender.Click += new System.EventHandler(this.label4_Click);
            // 
            // textAge
            // 
            this.textAge.Location = new System.Drawing.Point(159, 121);
            this.textAge.Name = "textAge";
            this.textAge.Size = new System.Drawing.Size(121, 20);
            this.textAge.TabIndex = 12;
            // 
            // comboGender
            // 
            this.comboGender.FormattingEnabled = true;
            this.comboGender.Items.AddRange(new object[] {
            "Мужской",
            "Женский"});
            this.comboGender.Location = new System.Drawing.Point(159, 94);
            this.comboGender.Name = "comboGender";
            this.comboGender.Size = new System.Drawing.Size(121, 21);
            this.comboGender.TabIndex = 13;
            // 
            // textSName
            // 
            this.textSName.Location = new System.Drawing.Point(159, 65);
            this.textSName.Name = "textSName";
            this.textSName.Size = new System.Drawing.Size(121, 20);
            this.textSName.TabIndex = 14;
            // 
            // textName
            // 
            this.textName.Location = new System.Drawing.Point(159, 35);
            this.textName.Name = "textName";
            this.textName.Size = new System.Drawing.Size(121, 20);
            this.textName.TabIndex = 15;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(391, 324);
            this.Controls.Add(this.textName);
            this.Controls.Add(this.textSName);
            this.Controls.Add(this.comboGender);
            this.Controls.Add(this.textAge);
            this.Controls.Add(this.laGender);
            this.Controls.Add(this.laAge);
            this.Controls.Add(this.laSName);
            this.Controls.Add(this.laName);
            this.Controls.Add(this.buSave);
            this.Controls.Add(this.laWord);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.buNextWord);
            this.Controls.Add(this.laInfo);
            this.Controls.Add(this.buStartTest);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.Text = "Ассоциативный тест";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buStartTest;
        private System.Windows.Forms.Label laInfo;
        private System.Windows.Forms.Button buNextWord;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label laWord;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button buSave;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Windows.Forms.Label laName;
        private System.Windows.Forms.Label laSName;
        private System.Windows.Forms.Label laAge;
        private System.Windows.Forms.Label laGender;
        private System.Windows.Forms.TextBox textAge;
        private System.Windows.Forms.ComboBox comboGender;
        private System.Windows.Forms.TextBox textSName;
        private System.Windows.Forms.TextBox textName;
    }
}


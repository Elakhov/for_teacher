﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections;
using Xceed.Words.NET;
using System.Drawing;



namespace TestAsoc
{
    class Test
    {
        private int value = 0;
        private int columnCount = 0;
        private string fName;
        private string sName;
        private int age;
        private string gender;

        public string fileName
        {
            get
            {
                return @"C: \Users\alel\Desktop\test.docx";
            }
        }

        ArrayList words = new ArrayList();
        StringBuilder strings = new StringBuilder();



        public void ReadLines(ArrayList list, string fName, string sName, int age, string gender)
        {
            StreamReader reader = new StreamReader(@"C: \Users\alel\Desktop\wordtest.txt", Encoding.Default);
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                string[] wordarr = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < wordarr.Length; i++)
                {
                    list.Add(wordarr[i]);
                }
            }
            reader.Close();
            this.fName = fName;
            this.sName = sName;
            this.age = age;
            this.gender = gender;
        }

        public void AddTableInDoc ()
        {
            int rows = (int)Math.Ceiling((double)(words.Count) / 4);

            using (DocX document = DocX.Create(fileName))
            {
                document.InsertParagraph(fName + " " + sName + ", Возраст: " + age + ", Пол: " + gender).FontSize(15d).SpacingAfter(20d).Alignment = Alignment.center;
                var p = document.InsertParagraph("Таблица Стимул-Реакция");
                var t = document.AddTable(2, 4);
                t.Alignment = Alignment.center;

                for (var i = 0; i < rows; i++)
                {
                    for (var j = 0; j < 4; j++)
                    {
                        if (columnCount > words.Count - 1)
                            break;
                        t.Rows[i].Cells[j].Paragraphs[0].Append(words[columnCount].ToString()).FontSize(14d);
                        columnCount++;
                    }
                }

                var blankBorder = new Border(BorderStyle.Tcbs_dotted, 0, 0, Color.Black);
                t.SetBorder(TableBorderType.Bottom, blankBorder);
                t.SetBorder(TableBorderType.InsideH, blankBorder);
                t.SetBorder(TableBorderType.Top, blankBorder);

                p.InsertTableAfterSelf(t);
                document.Save();
            }
        }

        public void AppendText(string word, string answer)
        {
            words.Add(word.ToUpper() + " - " + answer.ToLower());
        }
    }
}

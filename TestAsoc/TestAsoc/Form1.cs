﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Collections;


namespace TestAsoc
{
    public partial class Form1 : Form
    {

        Test test = new Test();
        ArrayList list = new ArrayList();
        public int count = 0;

        public Form1()
        {
            InitializeComponent();
            textName.Focus();
            textBox1.Visible = false;
            laWord.Visible = false;
            laInfo.Visible = false;
            buSave.Visible = false;
            buNextWord.Visible = false;
            buStartTest.Visible = true;
            buSave.Visible = false;
        }

        private void buOpenFile_Click(object sender, EventArgs e)
        {
         
        }

        private void buSave_Click(object sender, EventArgs e)
        {
                test.AddTableInDoc();
        }

        private void buNextWord_Click(object sender, EventArgs e)
        {
            test.AppendText(list[count].ToString(), textBox1.Text);
            textBox1.Clear();
            textBox1.Focus();
            if (count < list.Count - 1)
            {
                count++;
                laWord.Text = list[count].ToString().ToUpper();
            }
            else
            {
                buNextWord.Visible = false;
                laInfo.Text = "Тест закончен, сохраните результат";
                buSave.Visible = true;
                textBox1.Visible = false;
                laWord.Visible = false;
            }
            
        }

        private void buStartTest_Click(object sender, EventArgs e)
        {
            test.ReadLines(list, textName.Text, textSName.Text, int.Parse(textAge.Text), comboGender.SelectedItem.ToString());
            laWord.Text = list[count].ToString().ToUpper();
            textBox1.Visible = true;
            laWord.Visible = true;
            laInfo.Visible = true;
            buNextWord.Visible = true;
            buStartTest.Visible = false;
            textBox1.Focus();
            laName.Visible = false;
            laSName.Visible = false;
            laGender.Visible = false;
            laAge.Visible = false;
            textName.Visible = false;
            textSName.Visible = false;
            textAge.Visible = false;
            comboGender.Visible = false;
        }

        private void laOpenFile_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void buPrint_Click(object sender, EventArgs e)
        {
         
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Neuro
{
    public partial class Form1 : Form
    {

        Neuron NB;

        public Form1()
        {
            InitializeComponent();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Открыть изображение";
            dialog.Filter = "img files (*.bmp)|*.bmp";

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                imgToRecog.Image = Image.FromFile(dialog.FileName);
            }
            else 
            {
                MessageBox.Show("Вы не открыли файл");
            }

            dialog.Dispose();
        }

        private void buLearn_Click(object sender, EventArgs e)
        {
            int size = Convert.ToInt32(tbSize.Text);
            Bitmap img = imgToRecog.Image as Bitmap;
            int[] inputs = new int[size];

            int i = 0;
            for (int x = 0; x < img.Width; x++)
            {
                for (int y = 0; y < img.Height; y++)
                {
                    int color = img.GetPixel(x, y).R;

                    if (color >= 255) color = 0;
                    else color = 1;
                    inputs[i] = color;
                    i++;
                }
            }

            NB.AddInputs(inputs);
            NB.Sums();
            bool result = NB.Rezult();
            StringBuilder str = NB.GetInfo();

            if (result) str.AppendLine("True, Sum = " + NB.sum.ToString());
            else str.AppendLine("False, Sum = " + NB.sum.ToString());

            tbResults.Text = str.ToString();

            using (StreamWriter sw = new StreamWriter(@"C:\Users\alel\Desktop\weights.txt", false, System.Text.Encoding.Default))
            {
                string[] weights = new string[size];
                for (int j = 0; j < NB.weight.Length; j++)
                {
                    weights[j] = NB.weight[j].ToString();
                }
                string weigthToSave = String.Join(" ", weights);
                sw.Write(weigthToSave);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int size = Convert.ToInt32(tbSize.Text);
            int rate = Convert.ToInt32(tbRate.Text);
            NB = new Neuron(size, rate);
            openWeights.Title = "Укажите файл весов";
            openWeights.Filter = "txt (*.txt)|*.txt";
            openWeights.ShowDialog();
            string path = openWeights.FileName;
            string line;
            string[] weights;
            StreamReader sr = File.OpenText(path);

            while( (line = sr.ReadLine()) != null )
            {
                weights = line.Split(' ');

                for (int i = 0; i < weights.Length; i++)
                {
                    NB.weight[i] = Convert.ToInt32(weights[i]);
                }
            }

            sr.Close();
           
            MessageBox.Show("Настройки сохранены");
        }

        private void buTrue_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Можете открыть другое изображение");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (NB.Rezult()) NB.Minus();
            else NB.Plus();
            buLearn_Click(sender, e);
        }
    }
}

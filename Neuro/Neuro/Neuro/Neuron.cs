﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Neuro
{
    class Neuron
    {
        public int[] input;
        public int[] weight;
        public int sum;
        int rate;
        public int limit = 9;

        //ИНИЦИАЛИЗАЦИЯ НЕЙРОНА
        public Neuron(int size, int rate)
        {
            weight = new int[size];

            this.rate = rate;
        }

        //ФОРМИРОВАНИЕ ВХОДОВ
        public void AddInputs(int[] input)
        {
            this.input = input;
        }

        //СУММИРОВАНИЕ 
        public double Sums()
        {
            sum = 0;
            for (int i = 0; i < weight.Length; i++)
            {
                sum += weight[i] * input[i];
            }

            return sum;
        }

        //РЕЗУЛЬТАТ
        public bool Rezult()
        {
            return sum >= limit;
        }

        //СТРОКА ДЛЯ ВЫВОДА ИНФОРМАЦИИ
        public StringBuilder GetInfo()
        {
            StringBuilder str = new StringBuilder();

            str.AppendLine("Текущие веса");

            for (int i = 0; i < weight.Length; i++)
            {
                str.AppendLine("W" + (i + 1) + " " + weight[i]);
            }

            return str;
        }

        //УМЕНЬШЕНИЕ ВЕСОВ
        public void Minus()
        {
            int delt = 0 - 1;
            for (int i = 0; i < weight.Length; i++)
            {
                weight[i] = weight[i] + (delt * input[i] * rate);
            }
        }

        //УВЕЛЕЧЕНИЕ ВЕСОВ
        public void Plus()
        {
            int delt = 1 - 0;
            for (int i = 0; i < weight.Length; i++)
            {
                weight[i] = weight[i] + (delt * input[i] * rate);
            }
        }
    }
}
